resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/8"
}

resource "aws_subnet" "public" {
  vpc_id = aws_vpc.main.id
  cidr_block = "10.1.1.0/24"
}


